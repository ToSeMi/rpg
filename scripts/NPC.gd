extends Node2D

@export var quest_name : String

@export var quest_line : String
@export var inbetween : String
@export var finished  : String

enum QUEST_STATE {
	inactive,
	active,
	finished
}

# Called when the node enters the scene tree for the first time.
func _ready():
	if(!PlayerState.quests.has(quest_name)):
		PlayerState.quests[quest_name] = QUEST_STATE.inactive
	if(PlayerState.quests[quest_name] == QUEST_STATE.active):
		$CanvasLayer/TextureRect/Label2.set_text(inbetween)
	else:
		$CanvasLayer/TextureRect/Label2.set_text(quest_line)

func _on_area_2d_area_entered(area):
	$CanvasLayer.show()


func _on_area_2d_area_exited(area):
	$CanvasLayer.hide()


func _on_button_button_down():
	if(PlayerState.quests[quest_name] != QUEST_STATE.active && PlayerState.quests[quest_name] != QUEST_STATE.finished):
		PlayerState.quests[quest_name] = QUEST_STATE.active
		$"CanvasLayer/TextureRect/Label2".set_text(inbetween)

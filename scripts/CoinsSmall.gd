extends Area2D

@export var value : int
@export var curve : Curve
func _on_Coins_area_entered(area: Area2D):
	if(area is Player):
		area.collect_coins(value)
		#area.amount_of_coins+=value
		queue_free()
var t = 0		
var wanted_scale : Vector2
func _ready():
	wanted_scale = self.scale	

func _process(delta):
	t+= delta
	self.scale = wanted_scale * curve.sample_baked(t)
	if(t >= 1): t = 0
#	print(curve.sample_baked(t)) 	
		
func area_entered(area: Area2D):
	pass

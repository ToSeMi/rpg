class_name Cactus extends Area2D

func _on_Cactus_area_entered(area: Area2D):
	if(area is Player):
		area.get_hurt(1)
		area.position = area.position + (-1*area.last_dir*area.TILE_SIZE)

class_name Enemy
extends Area2D

@export var max_health : int
var current_health : int

var prev_position : Vector2
var attacking : bool = false
var target_position : Vector2

func attack():
	print("oh no player is here")
	
	#waiting = false
# Called when the node enters the scene tree for the first time.
func _ready():
	current_health = max_health
	$"Timer".set_wait_time(2)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#position += $RayCast2D.position
	if(current_health <= 0):
		queue_free()

func _on_area_entered(area):
	if(area is Player):
		if (attacking && $Timer.is_stopped()):
			area.get_hurt(10)
			position = prev_position
			print("aaaaa")
			attacking = false
		else:
			current_health -= 10
			area.position = area.position + (-1*area.last_dir*area.TILE_SIZE)
			$RayCast2D.position = area.position - self.position
			self.prev_position = position
			attacking = true
			$Timer.start()
	else:
		attacking = false

func _on_timer_timeout():
	if(attacking):
		self.position += $RayCast2D.position
	

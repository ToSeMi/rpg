extends Node

@export var numOfCoins : Label
@export var healthBar : ProgressBar

func update_health(value : int = 1):
	healthBar.value-=value

func update_coins(value : int):	
	numOfCoins.set_text("Coins: "+ str(value))

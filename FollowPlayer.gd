extends Camera2D

@export var player : Area2D
const speed := 0.1
func _process(delta):
	self.position = lerp(self.position, player.position,speed)
